import boto3

def detect_labels(photo, bucket):

    client=boto3.client('rekognition','us-west-2')

    response = client.detect_labels(Image={'S3Object':{'Bucket':bucket,'Name':photo}})
  
    print('Detected labels for ' + photo) 
    for label in response['Labels']:
        print ("Label: " + label['Name'])
        print ("Confidence: " + str(label['Confidence']))

    return len(response['Labels'])


def main():
    bucket="reko2-jmi"
    image="markets.jpg"
    label_count=detect_labels(image, bucket)
    print("Labels detected: " + str(label_count))
    print()   


if __name__ == "__main__":
    main()


