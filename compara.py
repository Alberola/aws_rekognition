import boto3
import json

client_reko=boto3.client("rekognition","us-west-2")
bucket="reko3-jmi"

def compare_faces(source, target):

    response=client_reko.compare_faces(SimilarityThreshold=1,
                                  SourceImage={'S3Object':{'Bucket':bucket,'Name':source}},
                                  TargetImage={'S3Object':{'Bucket':bucket,'Name':target}})
    
    for faceMatch in response['FaceMatches']:
        position = faceMatch['Face']['BoundingBox']
        similarity = str(faceMatch['Similarity'])
        print('The face at ' +
               str(position['Left']) + ' ' +
               str(position['Top']) +
               ' matches with ' + similarity + '% confidence')

    return len(response['FaceMatches'])

def main():
    source_file='parejo4.png'
    target_file='parejo1.png'
    face_matches=compare_faces(source_file, target_file)
    print("Face matches: " + str(face_matches))


if __name__ == "__main__":
    main()

